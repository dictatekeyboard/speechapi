import requests
import sys
import time

max_errors = 10
errors = 0
lineno = 0
for line in open(sys.argv[1]):
    line = line.strip()
    lineno += 1
    payload = {'snippet': line, 'lineno': lineno, 'fname': sys.argv[1]}
    print "processing line %d" %lineno
    if errors >= max_errors: break
    while errors <= max_errors:
        resp = requests.post('https://api.hameed.info/speech/v1.0/snippets/', json=payload)
        if resp.status_code in [201, 409]: break
        sys.stderr.write('error code is %d\n' %resp.status_code)
        time.sleep(1)
        errors += 1
