"""log and snippet tables

Revision ID: 7e669cd9ac2c
Revises: 
Create Date: 2020-05-02 14:49:47.027730

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7e669cd9ac2c'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('snippet',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('fname', sa.String(length=255), nullable=False),
    sa.Column('lineno', sa.Integer(), nullable=True),
    sa.Column('snppt', sa.String(length=255), nullable=False),
    sa.Column('status', sa.String(length=1), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('fname', 'lineno', name='_fname_lineno_uc')
    )
    op.create_table('log',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('snippet_id', sa.Integer(), nullable=True),
    sa.Column('entry', sa.String(length=255), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['snippet_id'], ['snippet.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('log')
    op.drop_table('snippet')
    # ### end Alembic commands ###
