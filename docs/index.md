# Documentation

This is the top level documentation page.

The following pages are available:

* [Misrecognized][misrecognized]
* [Snippets][snippets]
* [Transcriber][transcriber]


## Definitions

* ```gold data```: a set of human verified audio segments and their textual transcriptions.
* ```snippet```: A small section of text, provided for training purposes, is used by the trainer application to record matching audio, for making gold data.


## Useful references:

### http status codes

Where possible, we should return a sensable http status code, supplemented by information in the body.
This [http status codes][httpstatuscodes]  page might be handy.

### Resty

[Resty] is a tiny script wrapper for curl.
It provides a simple, concise shell interface for interacting with REST services.

Where we provide command line examples Resty is used to make the examples clean and concise.

[httpstatuscodes]: https://restfulapi.net/http-status-codes/
[misrecognized]: misrecognized
[resty]: https://github.com/micha/resty
[snippets]: snippets
[transcriber]: transcriber
